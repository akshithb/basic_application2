package com.example.all_the_feat

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity2 : AppCompatActivity() {

    private var layoutmanager : RecyclerView.LayoutManager? = null
    private var adapter: RecyclerView.Adapter<RecyclerAdapter.ViewHolder>? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        layoutmanager = LinearLayoutManager(this)
        var rvid = findViewById<RecyclerView>(R.id.rvone)
        rvid.layoutManager=layoutmanager
        adapter=RecyclerAdapter()

        rvid.adapter=adapter


        var intent =intent
        var name = intent.getStringExtra("name")
        val yob = intent.getStringExtra("yob")
        var yob2=0
        if (yob!==null) {yob2=yob.toInt()}

        var ans = 2021-yob2
        var tvans=findViewById<TextView>(R.id.tvage)

        tvans.text="Hey "+ name +" your age is "+ans

    }
}






