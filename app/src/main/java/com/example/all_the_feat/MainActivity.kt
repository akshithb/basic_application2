package com.example.all_the_feat

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var name = findViewById<EditText>(R.id.etname)
        var yob = findViewById<EditText>(R.id.etyob)
        var findage = findViewById<Button>(R.id.btnfindage)

        findage.setOnClickListener{
            var p_name = name.text.toString();
            var p_yob = yob.text.toString();
            var intent = Intent(this,MainActivity2::class.java)

            intent.putExtra("name",p_name)
            intent.putExtra("yob",p_yob)
            startActivity(intent);
        }

    }
}