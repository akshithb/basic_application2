package com.example.all_the_feat

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class RecyclerAdapter: RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

    private var cond_array = arrayOf(5,10,15,19,25,30,35,40,50,60,70)
    private var res_array = arrayOf("Baby","Kid","Bigger Kid","Teen","Youth","Young","A bit Old","Older","Oldman you are","About to die","Dead")

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerAdapter.ViewHolder {
        val v= LayoutInflater.from(parent.context).inflate(R.layout.card_view,parent,false)
        return ViewHolder(v)

    }

    override fun onBindViewHolder(holder: RecyclerAdapter.ViewHolder, position: Int) {
        holder.cond_statement.text="If your age is around "+cond_array[position]+" then you are"
        holder.result_statement.text=res_array[position]
    }

    override fun getItemCount(): Int {
        return cond_array.size
    }

    inner class ViewHolder(itemView:View) : RecyclerView.ViewHolder(itemView){

        var cond_statement : TextView
        var result_statement : TextView

        init {
            cond_statement = itemView.findViewById(R.id.tvcondition)
            result_statement = itemView.findViewById(R.id.tvresult)
        }


    }
}